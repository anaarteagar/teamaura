import React from 'react';
import { Link } from 'react-router-dom';
import loginImage from '../../assets/uteq3.png';
import './ImageLogin.css'; // Archivo CSS para estilos específicos si es necesario

const ImageLogin = () => {
  return (
    <div>
      <img src={loginImage} alt="Login" />
      {/* Agrega un enlace a la página de productos */}
      <div className="button-container">
        <Link to="/mostraradmisiones">
          <button className="ver-productos-btn">Nuestras Admisiones</button>
        </Link>
        <Link to="/mostrarofertas">
          <button className="ver-productos-btn">Nuestras Ofertas</button>
        </Link>
      </div>
    </div>
  );
};

export default ImageLogin;
